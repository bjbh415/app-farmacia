const Sequelize = require('sequelize')
const jsyaml = require('js-yaml')
const fs = require('fs')
const util = require('util')
const models = require('./model-structures')

let sqlize, SQDefs = {}
function connectDB () {    
  if(sqlize) return sqlize.sync()
  return new Promise((resolve, reject) => {
    fs.readFile('connect-mysql.yaml', 'utf8', (err, data) => {
      if (err) reject(err)
      else resolve(data)
    })
  })
  .then(textyaml => {
    return jsyaml.safeLoad(textyaml, 'utf8')
  })
  .then(params => {
    if(!sqlize)sqlize = new Sequelize(params.dbname, params.user, params.pass, params.params)
    if(Object.keys(SQDefs).length === 0){
      SQDefs.Medicines = sqlize.define('medicines', models.medicines)
      SQDefs.Labs = sqlize.define('labs', models.labs)
      SQDefs.Distrbtrs = sqlize.define('distrbtrs', models.distrbtrs)
      SQDefs.PharmaForms = sqlize.define('pharma_forms', models.pharmaForms)
      SQDefs.AdminWays = sqlize.define('admin_ways', models.adminWays)
      SQDefs.Drugs = sqlize.define('drugs', models.drugs)
      SQDefs.MedsDrugs = sqlize.define('meds_drugs', models.meds_drugs)
      SQDefs.MsuremntUnits = sqlize.define('msuremntUnits', models.msuremntUnits)
      SQDefs.Categories = sqlize.define('categories', models.categories)
      SQDefs.MedsCategs = sqlize.define('meds_categs', models.meds_categs)
      /* Medicines - Labs (1:M) */
      SQDefs.Labs.hasMany(SQDefs.Medicines, {foreignKey: 'idLab'})
      SQDefs.Medicines.belongsTo(SQDefs.Labs, {foreignKey: 'idLab'})
      
      /* Medicines - Distrbtrs (1:M) */
      SQDefs.Distrbtrs.hasMany(SQDefs.Medicines, {foreignKey: 'idDist'})
      SQDefs.Medicines.belongsTo(SQDefs.Distrbtrs, {foreignKey: 'idDist'})

      /* Medicines - PharmaForms (1:M) */
      SQDefs.PharmaForms.hasMany(SQDefs.Medicines, {foreignKey: 'idPharmaForm'})
      SQDefs.Medicines.belongsTo(SQDefs.PharmaForms, {foreignKey: 'idPharmaForm'})

      /* Medicines - AdminWays (1:M) */
      SQDefs.AdminWays.hasMany(SQDefs.Medicines, {foreignKey: 'idAdminWay'})
      SQDefs.Medicines.belongsTo(SQDefs.AdminWays, {foreignKey: 'idAdminWay'})

      /*Medicienes - MedsDrugs (1:M) */
      SQDefs.MsuremntUnits.hasMany(SQDefs.MedsDrugs, {foreignKey: 'idMsuremntUnit'})
      SQDefs.Medicines.belongsTo(SQDefs.MsuremntUnits, {foreignKey: 'idMsuremntUnit'})

      /*Medicines - Drugs (N:M)*/
      SQDefs.Medicines.belongsToMany(SQDefs.Drugs, {through: SQDefs.MedsDrugs, foreignKey: 'idMed'})
      SQDefs.Drugs.belongsToMany(SQDefs.Medicines, {through: SQDefs.MedsDrugs, foreignKey: 'idDrug'})

      /*Medicines - Categories (N:M) */
      SQDefs.Medicines.belongsToMany(SQDefs.Categories, {through: SQDefs.MedsCategs, foreignKey: 'idMed'})
      SQDefs.Categories.belongsToMany(SQDefs.Medicines, {through: SQDefs.MedsCategs, foreignKey: 'idCateg'})
    }
    return sqlize.sync()
  })
}
/* 
  +-------+
  | DRUGS |
  +-------+
*/
/* Create a new Drug */
exports.createDrug = function(name){
  return connectDB()
  .then(SQModel=>{
    return SQDefs.Drugs.create({name: name})
  })
}
/* Return all Drugs in Array from drugs.txt */
function getAllDrugsInArray (){
  let data = fs.readFileSync('default_data/drugs.txt', 'utf8')
  return data.split('\n')
}
/* Create several drugs*/
function createDrugs(names){
  return connectDB()
  .then(SQModel=>{
    let drugProms = []    
    names.forEach(name => {
      drugProms.push(
        SQDefs.Drugs.create({name: name})
      )
    })    
    return Promise.all(drugProms)
  })
}

/* Execute this to create all drugs fast in drugs.txt*/
function createAllDrugs(){
  createDrugs(getAllDrugsInArray())
  .then(drugs=>{
    console.log('all drugs created')
  })
  .catch(err=>{
    console.log(err)
  })
}
/* Find a Drug */
exports.findDrug = function(id){
  return connectDB()
  .then(SQModel=>{
    return SQDefs.Drugs.findById(id)
    .then(drug=>{
      if(!drug) throw new Error('Drug Not Found')
      return drug
    })
  })
}
/* Find all Drugs */
exports.findAllDrugs = function(){
  return connectDB()
  .then(SQModel=>{
    return SQDefs.Drugs.findAll()
    .then(drugs=>{
      if(!drugs) return []
      return drugs
    })
  })
}
/* Destroy a Drug */
exports.destroyDrug = function(id){
  return connectDB()
  .then(SQModel=>{
    return SQDefs.Drugs.findById(id)
    .then(drug=>{
      if(!drug) throw new Error('Drug Not Found')
      drug.destroy()      
    })
  })
}
/* Update a Drug */
exports.updateDrug = function(id, name){
  return connectDB()
  .then(SQModel=>{
    return SQDefs.Drugs.findById(id)
    .then(drug=>{
      if(!drug) throw new Error('Drug Not Found')
      return drug.updateAttributes({name:name})
    })
  })
}
/* 
  +------------+
  | ADMIN WAYS |
  +------------+
*/
/* Create a new adminWay */
exports.createAdminWay = function(way){
  return connectDB()
  .then(SQModel=>{
    return SQDefs.AdminWays.create({way: way})
  })
}
/* Return all AdminWays in Array from adminWays.txt */
function getAllAdminWaysInArray (){
  let data = fs.readFileSync('default_data/adminWays.txt', 'utf8')
  return data.split('\n')
}
/* Create several adminWays*/
function createAdminWays(ways){
  return connectDB()
  .then(SQModel=>{
    let adminWayProms = []    
    ways.forEach(way => {
      adminWayProms.push(
        SQDefs.AdminWays.create({way: way})
      )
    })    
    return Promise.all(adminWayProms)
  })
}
/* Execute this to create all adminWays fast in adminWays.txt*/
function createAllAdminWays(){
  createAdminWays(getAllAdminWaysInArray())
  .then(adminWay=>{
    console.log('all adminWays created')
  })
  .catch(err=>{
    console.log(err)
  })
}
/* Find a AdminWay */
exports.findAdminWay = function(id){
  return connectDB()
  .then(SQModel=>{
    return SQDefs.AdminWays.findById(id)
    .then(AdminWay=>{
      if(!AdminWay) throw new Error('AdminWay Not Found')
      return AdminWay
    })
  })
}
/* Find all AdminWays */
exports.findAllAdminWays = function(){
  return connectDB()
  .then(SQModel=>{
    return SQDefs.AdminWays.findAll()
    .then(AdminWays=>{
      if(!AdminWays) return []
      return AdminWays
    })
  })
}
/* Destroy a AdminWay */
exports.destroyAdminWay = function(id){
  return connectDB()
  .then(SQModel=>{
    return SQDefs.AdminWays.findById(id)
    .then(AdminWay=>{
      if(!AdminWay) throw new Error('AdminWay Not Found')
      AdminWay.destroy()      
    })
  })
}
/* Update a AdminWay */
exports.updateAdminWay = function(id, way){
  return connectDB()
  .then(SQModel=>{
    return SQDefs.AdminWays.findById(id)
    .then(AdminWay=>{
      if(!AdminWay) throw new Error('AdminWay Not Found')
      return AdminWay.updateAttributes({way:way})
    })
  })
}
/* 
  +----------------+
  | MSUREMNT UNITS |
  +----------------+
*/
/* Create a new MsuremntUnit */
exports.createMsuremntUnit = function(unit){
  return connectDB()
  .then(SQModel=>{
    return SQDefs.MsuremntUnits.create({unit: unit})
  })
}
/* Return all MsuremntUnits in Array from msuremntUnits.txt */
function getAllMsuremntUnitsInArray (){
  let data = fs.readFileSync('default_data/msuremntUnits.txt', 'utf8')
  return data.split('\n')
}

/* Create several MsuremntUnits*/
function createMsuremntUnits(units){
  return connectDB()
  .then(SQModel=>{
    let MsuremntUnitProms = []    
    units.forEach(unit => {
      MsuremntUnitProms.push(
        SQDefs.MsuremntUnits.create({unit: unit})
      )
    })    
    return Promise.all(MsuremntUnitProms)
  })
}

/* Execute this to create all MsurementUnits fast in msurementUnits.txt*/
function createAllMsuremntUnits(){
  createMsuremntUnits(getAllMsuremntUnitsInArray())
  .then(MsuremntUnit=>{
    console.log('all MsuremntUnits created')
  })
  .catch(err=>{
    console.log(err)
  })
}
/* Find a MsuremntUnit */
exports.findMsuremntUnit = function(id){
  return connectDB()
  .then(SQModel=>{
    return SQDefs.MsuremntUnits.findById(id)
    .then(MsuremntUnit=>{
      if(!MsuremntUnit) throw new Error('MsuremntUnit Not Found')
      return MsuremntUnit
    })
  })
}
/* Find all MsuremntUnits */
exports.findAllMsuremntUnits = function(){
  return connectDB()
  .then(SQModel=>{
    return SQDefs.MsuremntUnits.findAll()
    .then(MsuremntUnits=>{
      if(!MsuremntUnits) return []
      return MsuremntUnits
    })
  })
}

/* Destroy a MsuremntUnit */
exports.destroyMsuremntUnit = function(id){
  return connectDB()
  .then(SQModel=>{
    return SQDefs.MsuremntUnits.findById(id)
    .then(MsuremntUnit=>{
      if(!MsuremntUnit) throw new Error('MsuremntUnit Not Found')
      MsuremntUnit.destroy()      
    })
  })
}
/* Update a MsuremntUnit */
exports.updateMsuremntUnit = function(id, unit){
  return connectDB()
  .then(SQModel=>{
    return SQDefs.MsuremntUnits.findById(id)
    .then(MsuremntUnit=>{
      if(!MsuremntUnit) throw new Error('MsuremntUnit Not Found')
      return MsuremntUnit.updateAttributes({unit:unit})
    })
  })
}
/* 
  +------------+
  | CATEGORIES |
  +------------+
*/
/* Create a new Category */
exports.createCategory = function(categ){
  return connectDB()
  .then(SQModel=>{
    return SQDefs.Categories.create({categ: categ})
  })
}
/* Return all Category in Array from categories.txt */
function getAllCategoriesInArray (){
  let data = fs.readFileSync('default_data/categories.txt', 'utf8')
  return data.split('\n')
}
/* Create several Category*/
function createCategories(categs){
  return connectDB()
  .then(SQModel=>{
    let CategoryProms = []    
    categs.forEach(categ => {
      CategoryProms.push(
        SQDefs.Categories.create({categ: categ})
      )
    })    
    return Promise.all(CategoryProms)
  })
}
/* Execute this to create all categories fast in categories.txt*/
function createAllCategories(){
  createCategories(getAllCategoriesInArray())
  .then(Categories=>{
    console.log('all Categories created')
  })
  .catch(err=>{
    console.log(err)
  })
}
/* Find a Category */
exports.findCategory = function(id){
  return connectDB()
  .then(SQModel=>{
    return SQDefs.Categories.findById(id)
    .then(Category=>{
      if(!Category) throw new Error('Category Not Found')
      return Category
    })
  })
}
/* Find all Categories */
exports.findAllCategories = function(){
  return connectDB()
  .then(SQModel=>{
    return SQDefs.Categories.findAll()
    .then(Categories=>{
      if(!Categories) return []
      return Categories
    })
  })
}
/* Destroy a Category */
exports.destroyCategory = function(id){
  return connectDB()
  .then(SQModel=>{
    return SQDefs.Categories.findById(id)
    .then(Category=>{
      if(!Category) throw new Error('Category Not Found')
      Category.destroy()      
    })
  })
}
/* Update a Category */
exports.updateCategory = function(id, categ){
  return connectDB()
  .then(SQModel=>{
    return SQDefs.Categories.findById(id)
    .then(Category=>{
      if(!Category) throw new Error('Category Not Found')
      return Category.updateAttributes({categ:categ})
    })
  })
}
/* 
  +--------------+
  | PHARMA FORMS |
  +--------------+
*/
/* Create a new Drug */
exports.createPharmaForm = function(form){
  return connectDB()
  .then(SQModel=>{
    return SQDefs.PharmaForms.create({form: form})
  })
}
/* Return all PharmaForms in Array from PharmaForms.txt */
function getAllPharmaFormsInArray (){
  let data = fs.readFileSync('default_data/pharmaForms.txt', 'utf8')
  return data.split('\n')
}
/* Create several PharmaForms*/
function createPharmaForms(forms){
  return connectDB()
  .then(SQModel=>{
    let PharmaFormProms = []    
    forms.forEach(form => {
      PharmaFormProms.push(
        SQDefs.PharmaForms.create({form: form})
      )
    })    
    return Promise.all(PharmaFormProms)
  })
}
/* Execute this to create all PharmaForms fast in PharmaForms.txt*/
function createAllPharmaForms(){
  createPharmaForms(getAllPharmaFormsInArray())
  .then(PharmaForms=>{
    console.log('all PharmaForms created')
  })
  .catch(err=>{
    console.log(err)
  })
}
/* Find a PharmaForm */
exports.findPharmaForm = function(id){
  return connectDB()
  .then(SQModel=>{
    return SQDefs.PharmaForms.findById(id)
    .then(PharmaForm=>{
      if(!PharmaForm) throw new Error('PharmaForm Not Found')
      return PharmaForm
    })
  })
}
/* Find all PharmaForms */
exports.findAllPharmaForms = function(){
  return connectDB()
  .then(SQModel=>{
    return SQDefs.PharmaForms.findAll()
    .then(PharmaForms=>{
      if(!PharmaForms) return []
      return PharmaForms
    })
  })
}
/* Destroy a PharmaForm */
exports.destroyPharmaForm = function(id){
  return connectDB()
  .then(SQModel=>{
    return SQDefs.PharmaForms.findById(id)
    .then(PharmaForm=>{
      if(!PharmaForm) throw new Error('PharmaForm Not Found')
      PharmaForm.destroy()      
    })
  })
}
/* Update a PharmaForm */
exports.updatePharmaForm = function(id, form){
  return connectDB()
  .then(SQModel=>{
    return SQDefs.PharmaForms.findById(id)
    .then(PharmaForm=>{
      if(!PharmaForm) throw new Error('PharmaForm Not Found')
      return PharmaForm.updateAttributes({form:form})
    })
  })
}

/* 
  +------+
  | LABS |
  +------+
*/
/* Create a new Lab */
exports.createLab = function(name, rif, tlf, email, web, logo){
    return connectDB()
    .then(SQModel=>{
      return SQDefs.Labs.create({name, rif, tlf, email, web, logo})
    })
}
/* Find a Lab */
exports.findLab = function(id){
  return connectDB()
  .then(SQModel=>{
    return SQDefs.Labs.findById(id)
    .then(Lab=>{
      if(!Lab) throw new Error('Lab Not Found')
      return Lab
    })
  })
}
/* Find all Labs */
exports.findAllLabs = function(){
  return connectDB()
  .then(SQModel=>{
    return SQDefs.Labs.findAll()
    .then(Labs=>{
      if(!Labs) return []
      return Labs
    })
  })
}
/* Destroy a Lab */
exports.destroyLab = function(id){
  return connectDB()
  .then(SQModel=>{
    return SQDefs.Labs.findById(id)
    .then(Lab=>{
      if(!Lab) throw new Error('Lab Not Found')
      Lab.destroy()      
    })
  })
}
/* Update a lab */
exports.updateLab = function(id, name, rif, tlf, email, web, logo){
  return connectDB()
  .then(SQModel=>{
    return SQDefs.Labs.findById(id).then(Lab=>{
      if(!Lab) throw new Error('Lab Not Found')
      return Lab.updateAttributes({name, rif, tlf, email, web, logo})
    })
  })
}

/* 
  +-----------+
  | DISTRBTRS |
  +-----------+
*/
/* Create a new Distrbtr */
exports.createDistrbtr = function(name, rif, tlf, email, web, logo){
  return connectDB()
  .then(SQModel=>{
    return SQDefs.Distrbtrs.create({name, rif, tlf, email, web, logo})
  })
}
/* Find a Distrbtr */
exports.findDistrbtr = function(id){
return connectDB()
.then(SQModel=>{
  return SQDefs.Distrbtrs.findById(id)
  .then(Distrbtr=>{
    if(!Distrbtr) throw new Error('Distrbtr Not Found')
    return Distrbtr
  })
})
}
/* Find all Distrbtrs */
exports.findAllDistrbtrs = function(){
return connectDB()
.then(SQModel=>{
  return SQDefs.Distrbtrs.findAll()
  .then(Distrbtrs=>{
    if(!Distrbtrs) return []
    return Distrbtrs
  })
})
}
/* Destroy a Distrbtr */
exports.destroyDistrbtr = function(id){
return connectDB()
.then(SQModel=>{
  return SQDefs.Distrbtrs.findById(id)
  .then(Distrbtr=>{
    if(!Distrbtr) throw new Error('Distrbtr Not Found')
    Distrbtr.destroy()      
  })
})
}
/* Update a Distrbtr */
exports.updateDistrbtr = function(id, name, rif, tlf, email, web, logo){
return connectDB()
.then(SQModel=>{
  return SQDefs.Distrbtrs.findById(id).then(Distrbtr=>{
    if(!Distrbtr) throw new Error('Distrbtr Not Found')
    return Distrbtr.updateAttributes({name, rif, tlf, email, web, logo})
  })
})
}

/* 
  +-----------+
  | MEDICINES |
  +-----------+
*/
/* Create a new Medicine */
exports.createMedicine = function(codeBar, tradeName,idLab, idDist,
  idPharmaForm, warnings, cntrindications, idAdminWay, netCont, idMsuremntUnit, img){
    return connectDB()
    .then(SQModel=>{
      return SQDefs.Medicines.create({codeBar, tradeName,idLab, idDist,
        idPharmaForm, warnings, cntrindications, idAdminWay, netCont, idMsuremntUnit, img})
    })
}

/* Find a Medicine */
exports.findMedicine = function(id){
  return connectDB()
  .then(SQModel=>{
    return SQDefs.Medicines.findById(id, {
      include: [
        {model: SQDefs.Labs, attributes:['id','name','rif','tlf','email','web','logo']},
        {model: SQDefs.Distrbtrs, attributes:['id','name','rif','tlf','email','web','logo']},
        {model: SQDefs.PharmaForms, attributes:['id', 'form']},
        {model: SQDefs.MsuremntUnits, attributes:['id', 'unit']},
        {model: SQDefs.AdminWays, attributes:['id', 'way']},
        {model: SQDefs.Drugs},
        {model: SQDefs.Categories}
      ]
    })
    .then(Medicine=>{
      if(!Medicine) throw new Error('Medicine Not Found')
      return Medicine
    })
  })
}
/* Find all Medicines */
exports.findAllMedicines = function(){
return connectDB()
.then(SQModel=>{
  return SQDefs.Medicines.findAll({
    include: [
      {model: SQDefs.Labs, attributes:['id','name','rif','tlf','email','web','logo']},
      {model: SQDefs.Distrbtrs, attributes:['id','name','rif','tlf','email','web','logo']},
      {model: SQDefs.PharmaForms, attributes:['id', 'form']},
      {model: SQDefs.MsuremntUnits, attributes:['id', 'unit']},
      {model: SQDefs.AdminWays, attributes:['id', 'way'],required: true},
      {model: SQDefs.Drugs},
      {model: SQDefs.Categories}
    ]
  })
  .then(Medicines=>{
    if(!Medicines) return []
    return Medicines
  })
})
}
/* Destroy a Medicine */
exports.destroyMedicine = function(id){
return connectDB()
.then(SQModel=>{
  return SQDefs.Medicines.findById(id)
  .then(Medicine=>{
    if(!Medicine) throw new Error('Medicine Not Found')
    Medicine.destroy()      
  })
})
}
/* Update a Medicine */
exports.updateMedicine = function(id, codeBar, tradeName, idLab, idDist,
  idPharmaForm, warnings, cntrindications, idAdminWay, netCont, idMsuremntUnit, img){
return connectDB()
.then(SQModel=>{
  return SQDefs.Medicines.findById(id).then(Medicine=>{
    if(!Medicine) throw new Error('Medicine Not Found')
    return Medicine.updateAttributes({codeBar, tradeName,idLab, idDist,
      idPharmaForm, warnings, cntrindications, idAdminWay, netCont, idMsuremntUnit, img})
  })
})
}

/*
exports.createLab('Laboratorio Ejemplo', 'J123456789', '+58-4247359236', 'example@example.com', 'https://example.com', null)
exports.createDistrbtr('Distribuidor Ejemplo', 'J123456789', '+58-4247359236', 'example@example.com', 'https://example.com', null)
exports.createMedicine('0123456789123', 'Atamel', 1, 1, 1,
'Estas son alguans advertencias de ejemplo',
'Estas son unas contraindicaciones de ejemplo', 1,
'40', 1, 
null
)*/
/*
exports.updateMedicine(1,'0123456789123', 'Atamel', 1, 1, 1,
'Estas son alguans advertencias de ejemplo',
'Estas son unas contraindicaciones de ejemplo', 1,
'40', 1, 
null
)*/
/*
exports.findMedicine(1)
.then(m=>{
  console.log(
    m.tradeName+' | ', 
    m.codeBar+' | ', 
    m.warnings+' | ', 
    m.cntrindications+' | ',
    m.netCont+' | ',
    m.msuremntUnit.unit+' | ',
    m.lab.name+' | ',
    m.distrbtr.name+' | ',
    m.pharma_form.form+' | ',
    m.admin_way.way
  )
})
.catch(err=>{
  console.log(err)
})*/
/*exports.findMsuremntUnit(2)
.then(mu=>{
  console.log(mu)
})
.catch(err=>{
  console.log(err)
})*/