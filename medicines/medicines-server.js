const restify = require('restify')
const medModel = require('./medicines-sequelize')
const util = require('util')

/* 
  +-----------------------+
  | RESTIFY SERVER CONFIG |
  +-----------------------+
*/
let server = restify.createServer({
    version:'0.0.1',
    name: 'Medicines Server'
})

server.use(restify.plugins.acceptParser(server.acceptable));
server.use(restify.plugins.authorizationParser())
server.use(check)
server.use(restify.plugins.queryParser())
server.use(restify.plugins.bodyParser({
    mapParams:true
}))

server.listen(process.env.PORT_MEDICINES || 3333, 'localhost', ()=>{
    util.log(`${server.name} v${server.versions} is running in ${server.url}`)
})

/* 
  +-----------+
  | MEDICINES |
  +-----------+
*/
/*create medicine */
server.post('/create-medicine',(req, res, next)=>{
    medModel.createMedicine(req.params.codeBar, req.params.tradeName, req.params.idLab, req.params.idDist,
        req.params.idPharmaForm, req.params.warnings, req.params.cntrindications, req.params.idAdminWay,
        req.params.netCont, req.params.idMsuremntUnit, req.params.img)
    .then(med=>{
        res.send(med)
        next(false)
    })
    .catch(err=>{
        res.send(500, err)
        next(false)
    })
})

/*update medicine */
server.put('/update-medicine/:id',(req, res, next)=>{
    medModel.updateMedicine(req.params.id, req.params.codeBar, req.params.tradeName, req.params.idLab, req.params.idDist,
        req.params.idPharmaForm, req.params.warnings, req.params.cntrindications, req.params.idAdminWay,
        req.params.netCont, req.params.idMsuremntUnit, req.params.img)
    .then(med=>{
        res.send(med)
        next(false)
    })
    .catch(err=>{
        res.send(500, err)
        next(false)
    })
})
/*delete medicine */
server.post('/destroy-medicine/:id',(req, res, next)=>{
    medModel.destroyMedicine(req.params.id)
    .then(()=>{
        res.send(200)
        next(false)
    })
    .catch(err=>{
        res.send(500, err)
        next(false)
    })
})
/*find medicine */
server.get('/find-medicine/:id',(req, res, next)=>{
    medModel.findMedicine(req.params.id)
    .then(med=>{
        res.send(med)
        next(false)
    })
    .catch(err=>{
        res.send(500, err)
        next(false)
    })
})
/* find all medicines */
server.get('/find-all-medicines',(req, res, next)=>{
    medModel.findAllMedicines()
    .then(med=>{
        res.send(med)
        next(false)
    })
    .catch(err=>{
        res.send(500, err)
        next(false)
    })
})

let apiKeys = [
    {
        user:"them",
        key:"D4ED43C0-8BD6-4FE2-B358-7C0E230D11EF"
    }
]

function check(req, res, next){
    if(req.authorization){
        let found = false
        for(let auth of apiKeys){
            if(auth.key === req.authorization.basic.password &&
            auth.user === req.authorization.basic.username){
                found = true
                break
            }
        }
        if(found) next()
        else {
            res.send(401, new Error("Not authenticated"))
            util.log('Failed Authentication check' + util.inspect(req.authorization))
            next(false)
        }
    }else{
        console.log('No Authoritation')
        res.send(500, new Error('No Authoritation key'))
        next(false)
    }
}