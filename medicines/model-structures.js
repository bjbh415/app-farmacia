const Sequelize = require('sequelize')

exports.medicines = {
  id: {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
  codeBar: {type: Sequelize.STRING(13), unique: true, allowNull: false},
  tradeName: {type: Sequelize.STRING(60), unique: true, allowNull: false},
  //idLab: Sequelize.INTEGER,
  //idDist: Sequelize.INTEGER,
  //idPharmaForm: Sequelize.INTEGER,
  warnings: Sequelize.STRING,
  cntrindications: Sequelize.STRING,
  //idAdminWay: Sequelize.INTEGER,
  netCont: Sequelize.STRING(7),
  img: Sequelize.STRING
}

exports.labs = exports.distrbtrs = {
  id: {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
  name: {type: Sequelize.STRING(60), unique: true},
  rif: {type: Sequelize.STRING(10), unique: true},
  tlf: Sequelize.STRING(15),
  email: Sequelize.STRING,
  web: Sequelize.STRING,
  logo: Sequelize.STRING
}

exports.pharmaForms = {
  id: {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
  form: {type: Sequelize.STRING(60), unique: true}
}

exports.adminWays = {
  id: {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
  way: {type: Sequelize.STRING(60), unique: true}
}

exports.drugs = {
  id: {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
  name: {type: Sequelize.STRING(60), unique: true}
}

exports.meds_drugs = {
  id: {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
  //idMed: Sequelize.INTEGER,
  //idDrug: Sequelize.INTEGER,
  dose: Sequelize.STRING(7),
  //idMsuremntUnit: Sequelize.INTEGER
}

exports.msuremntUnits = {
  id: {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
  unit: Sequelize.STRING(4)
}

exports.categories = {
    id: {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
    categ: {type: Sequelize.STRING(60), unique: true}
}

exports.meds_categs = {
    id: {type: Sequelize.INTEGER, primaryKey: true, autoIncrement: true},
    //idMed: Sequelize.INTEGER,
    //idCateg: Sequelize.INTEGER
}